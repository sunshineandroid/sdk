package com.hfax.sdk.test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertFalse;

public class BaseTest {

    public AppiumDriver<WebElement> driver;

    @Before
    public void setUp() throws Exception {
        Map<String, String> env = System.getenv();

        String appPath = env.get("APP_PATH");
        assertEquals("App path is empty", appPath != null, true);

        File checkApkPath = new File(appPath);
        assertEquals("App file does not exist", checkApkPath.exists(), true);

        String serverPath = env.get("SERVER_PATH");
        if (serverPath == null || serverPath.isEmpty()) {
            serverPath = "http://127.0.0.1:4723/wd/hub";
        }

        String platform = env.get("PLATFORM");
        assertFalse("NO platform specified", platform != null && platform.isEmpty());

        if (platform == null || platform.isEmpty()) {
            platform = "Android";
            System.out.println("Platform is not specified, the default platform for use Android");
        }

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", platform);
        capabilities.setCapability("noSign", true);
        capabilities.setCapability("app", appPath);

        if (platform.toLowerCase().contains("android")) {
            driver = new AndroidDriver<>(new URL(serverPath), capabilities);
        } else {
            driver = new IOSDriver<>(new URL(serverPath), capabilities);
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public void tearDown() throws Exception {
        if (driver != null) {
            driver.quit();
        }
    }
}
