package com.hfax.sdk.test;

import com.hfax.sdk.manager.UserManager;
import com.hfax.sdk.page.AuthorizePage;
import com.hfax.sdk.page.BasePage;
import com.hfax.sdk.page.LoginPage;
import com.hfax.sdk.page.LoginSuccessPage;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

/**
 * Created by WangSummer on 16/9/27.
 */
public class 未授权入口测试 extends BaseTest {
    private LoginPage mLoginPage;
    private AuthorizePage mAuthorizePage;
    private BasePage mBasePage;
    private LoginSuccessPage mLoginSuccesssPage;

    public void setUp() throws Exception {
        super.setUp();
        mLoginPage = new LoginPage(driver);
        mAuthorizePage = new AuthorizePage(driver);
        mBasePage = new BasePage(driver);
        mLoginSuccesssPage = new LoginSuccessPage(driver);
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    @Test
    public void testAuthorize() throws Exception {
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mLoginPage);
        mLoginPage.entranceWallet(UserManager.INSTANCE.userCheDao);
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mAuthorizePage);
        mAuthorizePage.bt_HFAXUserServerProtocolClick();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mBasePage);
        mBasePage.bt_ReturnClick();
        mAuthorizePage.bt_WalletServerProtocolClick();
        mBasePage.bt_ReturnClick();
        mAuthorizePage.bt_AnthorizeLoginClick();
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(20, TimeUnit.SECONDS)), mLoginSuccesssPage);
        mLoginSuccesssPage.bt_CloseClick();

    }
}
