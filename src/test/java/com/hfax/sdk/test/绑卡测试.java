package com.hfax.sdk.test;

import com.hfax.sdk.manager.UserManager;
import com.hfax.sdk.page.BindCardPage;
import com.hfax.sdk.page.LoginPage;
import com.hfax.sdk.page.LoginSuccessPage;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

/**
 * Created by mac on 16/9/27.
 */
public class 绑卡测试 extends BaseTest {

    LoginPage mLoginPage;
    BindCardPage mBindCardPage;
    LoginSuccessPage mLoginSuccessPage;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        mLoginPage = new LoginPage(driver);
        mBindCardPage = new BindCardPage(driver);
        mLoginSuccessPage = new LoginSuccessPage(driver);
    }

    @Test
    public void bandcard() throws Exception {
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mLoginPage);
        mLoginPage.entranceWallet(UserManager.INSTANCE.userCheDao);

        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mLoginSuccessPage);
        mLoginSuccessPage.clickbutton();

        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mBindCardPage);
        mBindCardPage.bindcard(UserManager.INSTANCE.data);

    }
}
