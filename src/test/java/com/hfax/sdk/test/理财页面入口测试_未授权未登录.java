package com.hfax.sdk.test;

import com.hfax.sdk.manager.UserManager;
import com.hfax.sdk.page.LoginPage;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

/**
 * Created by WangSummer on 16/9/26.
 */
public class 理财页面入口测试_未授权未登录 extends BaseTest {
    private LoginPage mLoginPage;
    @Override
    public void setUp() throws Exception {
        super.setUp();
        mLoginPage = new LoginPage(driver);
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();
    }

    @Test
    public void testList() throws Exception {
        PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(5, TimeUnit.SECONDS)), mLoginPage);
        mLoginPage.entranceFinancial(UserManager.INSTANCE.userCheDao);
    }
}
