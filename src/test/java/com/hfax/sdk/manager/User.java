package com.hfax.sdk.manager;

/**
 * Created by trila on 16/3/4.
 */
public class User {
    private String channelSelect;
    private String phoneNumber;
    private String password;

    public User() {
    }

    public User(String channelSelect, String phoneNumber, String password) {
        this.channelSelect = channelSelect;
        this.phoneNumber = phoneNumber;
        this.password = password;
    }

    public String getChannelSelect() {
        return channelSelect;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getPassword() {
        return password;
    }
}
