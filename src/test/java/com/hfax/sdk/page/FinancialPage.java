package com.hfax.sdk.page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

/**
 * Created by WangSummer on 16/10/11.
 * <p>
 * 理财页面
 */
public class FinancialPage extends BasePage {

    /**
     * 筛选按钮
     */
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[1]/android.view.View[3]")
    WebElement bt_Screening;
    /**
     * 授权并登陆按钮(未授权未登录)
     */
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[9]")
    WebElement bt_authorizeAndLogin;



    public FinancialPage(AppiumDriver<?> driver) {
        super(driver);
    }

    public void Click_bt_Screening() {
        bt_Screening.click();
    }

    public void Click_bt_AuthorizeAndLogin() {
        bt_authorizeAndLogin.click();
    }


}
