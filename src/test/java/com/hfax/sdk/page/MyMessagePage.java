package com.hfax.sdk.page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

/**
 * Created by WangSummer on 16/10/13.
 * <p>
 * 我的消息页面
 */
public class MyMessagePage extends BasePage {

    /**
     * 全部已读按钮
     */
    @AndroidFindBy(xpath = "")
    WebElement bt_AllRead;

    public MyMessagePage(AppiumDriver<?> driver) {
        super(driver);
    }

    public void Click_bt_AllRead() {
        bt_AllRead.click();
    }
}
