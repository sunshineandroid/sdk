package com.hfax.sdk.page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;

/**
 * Created by WangSummer on 16/9/28.
 * <p>
 * 交易明细页面
 */
public class TransactionDetailsPage extends BasePage {

    /**
     * 筛选按钮
     **/
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[1]/android.view.View[2]")
    @iOSFindBy(xpath = "")
    WebElement bt_Screening;

    /**
     * 全部筛选按钮
     **/
    @AndroidFindBy(xpath = "")
    @iOSFindBy(xpath = "")
    WebElement bt_AllScreening;

    /**
     * 充值筛选按钮
     **/
    @AndroidFindBy(xpath = "")
    @iOSFindBy(xpath = "")
    WebElement bt_TopUpScreening;

    /**
     * 提现筛选按钮
     **/
    @AndroidFindBy(xpath = "")
    @iOSFindBy(xpath = "")
    WebElement bt_WithdrawalScreening;

    /**
     * 投资筛选按钮
     **/
    @AndroidFindBy(xpath = "")
    @iOSFindBy(xpath = "")
    WebElement bt_InvestmentScreening;

    /**
     * 回款筛选按钮
     **/
    @AndroidFindBy(xpath = "")
    @iOSFindBy(xpath = "")
    WebElement bt_TheReceivableScreening;

    /**
     * 消费筛选按钮
     **/
    @AndroidFindBy(xpath = "")
    @iOSFindBy(xpath = "")
    WebElement bt_ConsumptionScreening;

    /**
     * 其他筛选按钮
     **/
    @AndroidFindBy(xpath = "")
    @iOSFindBy(xpath = "")
    WebElement bt_OtherScreening;

    public TransactionDetailsPage(AppiumDriver<?> driver) {
        super(driver);
    }

    public void Click_bt_Screening() {
        bt_Screening.click();
    }

    public void Click_bt_AllScreening() {
        bt_AllScreening.click();
    }

    public void Click_bt_TopUpScreening() {
        bt_TopUpScreening.click();
    }

    public void Click_bt_WithdrawalScreening() {
        bt_WithdrawalScreening.click();
    }

    public void Click_bt_InvestmentScreening() {
        bt_InvestmentScreening.click();
    }

    public void Click_bt_TheReceivableScreening() {
        bt_TheReceivableScreening.click();
    }

    public void Click_bt_ConsumptionScreening() {
        bt_ConsumptionScreening.click();
    }

    public void Click_bt_OtherScreening() {
        bt_OtherScreening.click();
    }
}
