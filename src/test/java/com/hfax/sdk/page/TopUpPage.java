package com.hfax.sdk.page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

/**
 * Created by WangSummer on 16/10/13.
 * <p>
 * 充值界面(未绑卡)
 */
public class TopUpPage extends BasePage {

    /**
     * 帮助按钮
     */
    @AndroidFindBy(xpath = "")
    WebElement bt_Help;

    /**
     * 添加银行卡按钮
     */
    @AndroidFindBy(xpath = "")
    WebElement bt_AddBankCard;

    /**
     * 充值按钮
     */
    @AndroidFindBy(xpath = "")
    WebElement bt_TopUp;

    /**
     * 支付服务协议按钮
     */
    @AndroidFindBy(xpath = "")
    WebElement bt_PayServerProtocol;

    /**
     * 充值金额输入框
     */
    @AndroidFindBy(xpath = "")
    WebElement et_TopUpAmount;


    public TopUpPage(AppiumDriver<?> driver) {
        super(driver);
    }

    public void Click_bt_Help() {
        bt_Help.click();
    }

    public void Click_bt_AddBankCard() {
        bt_AddBankCard.click();
    }

    public void Click_bt_PayServerProtocol() {
        bt_PayServerProtocol.click();
    }

    public void Click_bt_TopUp(String amount) {
        et_TopUpAmount.clear();
        et_TopUpAmount.sendKeys(amount);
        bt_TopUp.click();
    }
}
