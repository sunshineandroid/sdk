package com.hfax.sdk.page;

import com.hfax.sdk.manager.User;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

/**
 * Created by WangSummer on 16/9/21.
 * <p>
 * 模拟第三方界面
 */
public class LoginPage extends BasePage {

    /**
     * 模拟第三方首界面
     * 依次是:渠道号选择,车道渠道,橙牛渠道,手机号,密码,登录,直接登录
     */
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.Spinner[1]/android.widget.TextView[1]")
    WebElement bt_selectChannel;
    @AndroidFindBy(xpath = "//android.widget.ListView[1]/android.widget.CheckedTextView[1]")
    WebElement bt_channelLane;
    @AndroidFindBy(xpath = "//android.widget.ListView[1]/android.widget.CheckedTextView[2]")
    WebElement bt_channelChengNiu;
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.EditText[1]")
    WebElement et_phoneNumber;
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.LinearLayout[3]/android.widget.EditText[1]")
    WebElement et_password;
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.Button[1]")
    WebElement bt_login;
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.Button[2]")
    WebElement bt_directLogin;

    /**
     * 进入sdk的三个入口
     * 依次是:钱包页面,理财页面,消费页面
     */
    @AndroidFindBy(xpath = "//android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.Button[1]")
    WebElement bt_wallet;
    @AndroidFindBy(xpath = "//android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.Button[2]")
    WebElement bt_financial;
    @AndroidFindBy(xpath = "//android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.Button[3]")
    WebElement bt_consume;

    /**
     * 获取的数据,展示不需填写
     * 依次是:用户ID,用户名,手机号,authKey,URL,channelKey,merchantId,merchantKey.
     */
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.EditText[1]")
    WebElement et_userID;
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.EditText[1]")
    WebElement et_userName;
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.LinearLayout[3]/android.widget.EditText[1]")
    WebElement et_userPhoneNumber;
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.LinearLayout[4]/android.widget.EditText[1]")
    WebElement et_authKey;
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.LinearLayout[5]/android.widget.TextView[2]")
    WebElement tv_channelKey;
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.LinearLayout[6]/android.widget.TextView[2]")
    WebElement tv_merchantID;
    @AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.LinearLayout[7]/android.widget.EditText[1]")
    WebElement et_baseUrl;

//    @AndroidFindBy(xpath = "//android.widget.LinearLayout[1]/android.widget.LinearLayout[8]/android.widget.TextView[2]")
//    WebElement tv_merchantKey;

    public LoginPage(AppiumDriver<?> driver) {
        super(driver);
    }

    protected void setTextView(final User user) throws Exception {
        bt_selectChannel.click();
        if (user.getChannelSelect().equals("chengniu")) {
            bt_channelChengNiu.click();
        } else {
            bt_channelLane.click();
        }
//        et_phoneNumber.clear();
//        et_phoneNumber.sendKeys(user.getPhoneNumber());
//        et_password.clear();
//        et_password.sendKeys(user.getPassword());
        bt_login.click();
        Thread.sleep(1000);
        et_authKey.clear();
        et_authKey.sendKeys("52c8e869fa2fbf5c79892cf3c1a460fd78de49fc49c58ce86eeace5cec33578d");

    }

    public void entranceWallet(final User user) throws Exception {
        setTextView(user);
        bt_wallet.click();
        Thread.sleep(5000);
    }

    public void entranceFinancial(final User user) throws Exception {
        setTextView(user);
        bt_financial.click();
        Thread.sleep(5000);
    }

    public void entranceConsume(final User user) throws Exception {
        setTextView(user);
        bt_consume.click();
        Thread.sleep(5000);
    }

}