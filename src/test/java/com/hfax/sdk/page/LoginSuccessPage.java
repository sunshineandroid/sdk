package com.hfax.sdk.page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;

/**
 * 登陆成功页面
 * Created by mac on 16/9/27.
 */
public class LoginSuccessPage extends BasePage {

    /**
     * 关闭按钮
     **/
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[1]/android.view.View[2]")
    @iOSFindBy(xpath = "")
    WebElement bt_Close;
    /**
     * 绑定银行卡按钮
     */
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[5]")
    WebElement bindCard;

    public LoginSuccessPage(AppiumDriver<?> driver) {
        super(driver);
    }

    public void clickbutton() {
        bindCard.click();
    }

    public void bt_CloseClick() {
        bt_Close.click();
    }
}
