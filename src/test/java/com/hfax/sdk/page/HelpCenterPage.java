package com.hfax.sdk.page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

/**
 * Created by WangSummer on 16/10/13.
 * <p>
 * 帮助中心页面
 */
public class HelpCenterPage extends BasePage {

    /**
     * 关于我们按钮
     */
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[1]/android.view.View[2]")
    WebElement bt_AboutUs;

    /**
     * 拨打客服热线按钮
     */
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[2]/android.view.View[3]")
    WebElement bt_CallPhone;

    /**
     * 银行卡认证按钮
     */
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[4]")
    WebElement bt_BankCardCertify;

    /**
     * 充值/提现按钮
     */
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[6]")
    WebElement bt_WithdrawalAndTopUp;

    /**
     * 投资按钮
     */
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[8]")
    WebElement bt_Investment;

    public HelpCenterPage(AppiumDriver<?> driver) {
        super(driver);
    }

    public void Click_bt_AboutUs() {
        bt_AboutUs.click();
    }

    public void Click_bt_CallPhone() {
        bt_CallPhone.click();
    }

    public void Click_bt_BankCardCertify() {
        bt_BankCardCertify.click();
    }

    public void Click_bt_WithdrawalAndTopUp() {
        bt_WithdrawalAndTopUp.click();
    }

    public void Click_bt_Investment() {
        bt_Investment.click();
    }

}
