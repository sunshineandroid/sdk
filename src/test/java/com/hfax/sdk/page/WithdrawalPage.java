package com.hfax.sdk.page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

/**
 * Created by WangSummer on 16/10/13.
 * <p>
 * 提现界面(未绑卡)
 */
public class WithdrawalPage extends BasePage {

    /**
     * 帮助按钮
     */
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[1]/android.view.View[3]")
    WebElement bt_Help;

    /**
     * 添加银行卡按钮
     */
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[2]")
    WebElement bt_AddBankCard;

    /**
     * 提现按钮
     */
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[3]")
    WebElement bt_Withdrawal;

    /**
     * 提现金额输入框
     */
    @AndroidFindBy(xpath = "//android.widget.ListView[1]/android.view.View[3]/android.widget.EditText[1]")
    WebElement et_WithdrawalAmount;

    public WithdrawalPage(AppiumDriver<?> driver) {
        super(driver);
    }

    public void Click_bt_Help() {
        bt_Help.click();
    }

    public void Click_bt_AddBankCard() {
        bt_AddBankCard.click();
    }

    public void Click_bt_WithdrawalAmount(String amount) {
        et_WithdrawalAmount.clear();
        et_WithdrawalAmount.sendKeys(amount);
        bt_Withdrawal.click();
    }
}
