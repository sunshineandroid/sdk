package com.hfax.sdk.page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;

/**
 * 钱包页面
 * Created by WangSummer on 16/9/28.
 */
public class WalletPage extends BasePage {
    /**
     * 交易明细按钮
     **/
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[1]/android.view.View[2]")
    @iOSFindBy(xpath = "")
    WebElement bt_TransactionDetails;

    /**
     * 关于我们按钮
     **/
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[2]")
    @iOSFindBy(xpath = "")
    WebElement bt_AboutUs;

    /**
     * 我的消息按钮
     **/
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[3]")
    @iOSFindBy(xpath = "")
    WebElement bt_MyNews;

    /**
     * 金额显示按钮
     **/
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[4]")
    @iOSFindBy(xpath = "")
    WebElement bt_MyAssets;


    /**
     * 我的资产按钮
     **/
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[6]")
    @iOSFindBy(xpath = "")
    WebElement bt_AmountDisplayed;

    /**
     * 提现按钮
     **/
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[8]")
    @iOSFindBy(xpath = "")
    WebElement bt_Withdrawal;

    /**
     * 充值按钮
     **/
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[9]")
    @iOSFindBy(xpath = "")
    WebElement bt_TopUp;

    /**
     * 投资理财按钮
     **/
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.widget.ListView[1]/android.view.View[1]")
    @iOSFindBy(xpath = "")
    WebElement bt_FinanceAndInvestment;

    /**
     * 投资明细按钮
     **/
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.widget.ListView[1]/android.view.View[2]")
    @iOSFindBy(xpath = "")
    WebElement bt_TheInvestmentInSubsidiary;

    /**
     * 实名认证按钮
     **/
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.widget.ListView[2]/android.view.View[1]")
    @iOSFindBy(xpath = "")
    WebElement bt_RealNameAuthentication;

    /**
     * 银行卡管理按钮
     **/
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.widget.ListView[2]/android.view.View[2]")
    @iOSFindBy(xpath = "")
    WebElement bt_ManagementOfBankCard;

    /**
     * 交易密码管理按钮
     **/
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.widget.ListView[2]/android.view.View[3]")
    @iOSFindBy(xpath = "")
    WebElement bt_TradingPasswordManagement;

    /**
     * 帮助中心按钮
     **/
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.widget.ListView[3]/android.view.View[1]")
    @iOSFindBy(xpath = "")
    WebElement bt_HelpCenter;

    public WalletPage(AppiumDriver<?> driver) {
        super(driver);
    }

    public void Click_bt_TransactionDetails() {
        bt_TransactionDetails.click();
    }

    public void Click_bt_AboutUs() {
        bt_AboutUs.click();
    }

    public void Click_bt_MyNews() {
        bt_MyNews.click();
    }

    public void Click_bt_MyAssets() {
        bt_MyAssets.click();
    }

    public void Click_bt_AmountDisplayed() {
        bt_AmountDisplayed.click();
    }

    public void Click_bt_Withdrawal() {
        bt_Withdrawal.click();
    }

    public void Click_bt_TopUp() {
        bt_TopUp.click();
    }

    public void Click_bt_FinanceAndInvestment() {
        bt_FinanceAndInvestment.click();
    }

    public void Click_bt_TheInvestmentInSubsidiary() {
        bt_TheInvestmentInSubsidiary.click();
    }

    public void Click_bt_RealNameAuthentication() {
        bt_RealNameAuthentication.click();
    }

    public void Click_bt_ManagementOfBankCard() {
        bt_ManagementOfBankCard.click();
    }

    public void Click_bt_TradingPasswordManagement() {
        bt_TradingPasswordManagement.click();
    }

    public void Click_bt_HelpCenter() {
        bt_HelpCenter.click();
    }
}
