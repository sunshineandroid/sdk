package com.hfax.sdk.page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

/**
 * Created by WangSummer on 16/10/13.
 * <p>
 * 投资明细界面
 */
public class InvestmentDetailsPage extends BasePage {

    /**
     * 进行中按钮
     */
    @AndroidFindBy(xpath = "")
    WebElement bt_Ongoing;

    /**
     * 已还款按钮
     */
    @AndroidFindBy(xpath = "")
    WebElement bt_HasBeenPayment;

    /**
     * 立即去投资按钮
     */
    @AndroidFindBy(xpath = "")
    WebElement bt_ImmediatelyInvestment;

    public InvestmentDetailsPage(AppiumDriver<?> driver) {
        super(driver);
    }

    public void Click_bt_Ongoing(){
        bt_Ongoing.click();
    }

    public void Click_bt_HasBeenPayment(){
        bt_HasBeenPayment.click();
    }

    public void Click_bt_ImmediatelyInvestment(){
        bt_ImmediatelyInvestment.click();
    }
}
