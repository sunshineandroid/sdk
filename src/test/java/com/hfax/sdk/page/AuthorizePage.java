package com.hfax.sdk.page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;

/**
 * 授权页面
 * Created by WangSummer on 16/9/27.
 */
public class AuthorizePage extends BasePage {
    /**
     * 惠金所用户服务协议按钮
     **/
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[6]/android.view.View[1]")
    @iOSFindBy(xpath = "")
    WebElement bt_HFAXUserServerProtocol;

    /**
     * 钱包服务协议
     **/
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[6]/android.view.View[2]")
    @iOSFindBy(xpath = "")
    WebElement bt_WalletServerProtocol;

    /**
     * 复选框按钮
     **/
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[7]")
    @iOSFindBy(xpath = "")
    WebElement check_Box;

    /**
     * 授权并登录按钮
     **/
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[8]")
    @iOSFindBy(xpath = "")
    WebElement bt_AnthorizeLogin;

    public AuthorizePage(AppiumDriver<?> driver) {
        super(driver);
    }

    public void bt_HFAXUserServerProtocolClick() {
        bt_HFAXUserServerProtocol.click();
    }

    public void bt_WalletServerProtocolClick() {
        bt_WalletServerProtocol.click();
    }

    public void check_BoxSelect() {
        check_Box.click();
    }

    public void bt_AnthorizeLoginClick() {
        bt_AnthorizeLogin.click();
    }
}
