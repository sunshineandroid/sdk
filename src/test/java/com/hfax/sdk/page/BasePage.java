package com.hfax.sdk.page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;

public class BasePage {
    /**
     * 返回按钮
     **/
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[1]/android.view.View[1]")
    @iOSFindBy(xpath = "")
    WebElement bt_Return;

    protected AppiumDriver<?> driver;

    public BasePage(AppiumDriver<?> driver) {
        this.driver = driver;
    }

    public void bt_ReturnClick() {
        bt_Return.click();
    }
}
