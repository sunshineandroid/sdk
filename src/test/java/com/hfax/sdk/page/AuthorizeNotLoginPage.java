package com.hfax.sdk.page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.WebElement;

/**
 * 已授权未登录页面
 * Created by WangSummer on 16/9/27.
 */
public class AuthorizeNotLoginPage extends BasePage {

    /**
     * 验证码输入框
     **/
    @AndroidFindBy(xpath = "//android.widget.ListView[1]/android.view.View[1]/android.widget.EditText[1]")
    @iOSFindBy(xpath = "")
    WebElement et_VerificationCode;

    /**
     * 动态吗输入框
     **/
    @AndroidFindBy(xpath = "//android.widget.ListView[1]/android.view.View[2]/android.widget.EditText[1]")
    @iOSFindBy(xpath = "")
    WebElement et_DynamicCode;

    /**
     * 登录密码输入框
     **/
    @AndroidFindBy(xpath = "//android.widget.ListView[1]/android.view.View[3]/android.widget.EditText[1]")
    @iOSFindBy(xpath = "")
    WebElement et_LoginPassword;

    /**
     * 获取动态码按钮
     **/
    @AndroidFindBy(xpath = "//android.widget.ListView[1]/android.view.View[2]/android.view.View[1]")
    @iOSFindBy(xpath = "")
    WebElement bt_DynamicCode;

    /**
     * 忘记登录密码按钮
     **/
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.widget.ListView[1]/android.view.View[4]")
    @iOSFindBy(xpath = "")
    WebElement bt_ForgetLoginPassword;

    /**
     * 登陆按钮
     **/
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[3]")
    @iOSFindBy(xpath = "")
    WebElement bt_Login;

    /**
     * 验证码刷新按钮
     **/
    @AndroidFindBy(xpath = "//android.widget.ListView[1]/android.view.View[1]/android.widget.Image[1]")
    @iOSFindBy(xpath = "")
    WebElement bt_RefreshVerificationCode;

    public AuthorizeNotLoginPage(AppiumDriver<?> driver) {
        super(driver);
    }

    public void Login() {
        bt_RefreshVerificationCode.click();
        et_VerificationCode.sendKeys("acdf");
        bt_DynamicCode.click();
        et_DynamicCode.sendKeys("1234");
        et_LoginPassword.sendKeys("sdfgh");
        bt_Login.click();
    }

    public void setBt_ForgetLoginPassword() {
        bt_ForgetLoginPassword.click();
    }

}
