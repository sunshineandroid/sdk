package com.hfax.sdk.page;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

/**
 * Created by WangSummer on 16/10/13.
 * <p>
 * 我的资产页面
 */
public class MyPropertyPage extends BasePage {

    /**
     * 交易明细按钮
     */
    @AndroidFindBy(xpath = "")
    WebElement bt_TransactionDetails;

    /**
     * 立即充值按钮
     */
    @AndroidFindBy(xpath = "")
    WebElement bt_ImmediatelyTopUp;

    public MyPropertyPage(AppiumDriver<?> driver) {
        super(driver);
    }

    public void Click_bt_TransactionDetails() {
        bt_TransactionDetails.click();
    }

    public void Click_bt_ImmediatelyTopUp() {
        bt_ImmediatelyTopUp.click();
    }
}
