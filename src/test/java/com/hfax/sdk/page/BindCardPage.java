package com.hfax.sdk.page;

import com.hfax.sdk.manager.Data;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;

/**
 * 绑定银行卡页面
 * Created by mac on 16/9/27.
 */
public class BindCardPage extends BasePage {
    /**
     * 卡号输入框
     */
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[3]/android.widget.EditText[1]")
    WebElement et_CardNumber;

    /**
     * 手机号输入框
     */
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[4]/android.widget.EditText[1]")
    WebElement et_PhoneNumber;

    /**
     * 下一步按钮
     */
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[6]")
    WebElement bt_next;

    /**
     * 支付服务协议链接
     */
    @AndroidFindBy(xpath = "//android.webkit.WebView[1]/android.view.View[5]/android.view.View[2]")
    WebElement link_protocol;

    public BindCardPage(AppiumDriver<?> driver) {
        super(driver);
    }

    public void bindcard(Data data) {
        et_CardNumber.clear();
        et_CardNumber.sendKeys(data.getCardnumber());
        et_PhoneNumber.clear();
        et_PhoneNumber.sendKeys(data.getPhonenumber());
        bt_next.click();
    }
}
